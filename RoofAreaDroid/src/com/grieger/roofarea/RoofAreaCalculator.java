package com.grieger.roofarea;

import android.app.Activity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.common.collect.ImmutableList;
import com.grieger.roofarea.lib.RoofArea;
import com.grieger.roofarea.lib.imgproc.Canny;
import com.grieger.roofarea.lib.imgproc.GaussianBlur;
import com.grieger.roofarea.lib.imgproc.Hough;
import com.grieger.roofarea.lib.primitives.Line;
import org.opencv.core.Mat;

import java.util.HashMap;

public class RoofAreaCalculator {
  Activity ctx;

  public RoofAreaCalculator(Activity ctx) {
    this.ctx = ctx;
  }

  public HashMap<String, Mat> apply() {
    TextView roofGaussWidthInput = (TextView) ctx.findViewById(R.id.roofKernelWidth);
    Integer roofGaussWidth = Integer.parseInt(roofGaussWidthInput.getText().toString());

    TextView roofGaussHeightInput = (TextView) ctx.findViewById(R.id.roofKernelHeight);
    Integer roofGaussHeight = Integer.parseInt(roofGaussHeightInput.getText().toString());

    TextView roofSigmaInput = (TextView) ctx.findViewById(R.id.roofSigma);
    Integer roofGaussSigma = Integer.parseInt(roofSigmaInput.getText().toString());

    SeekBar roofCannyLowInput = (SeekBar) ctx.findViewById(R.id.roofCannyLowThreshold);
    Integer roofCannyLow = roofCannyLowInput.getProgress();

    SeekBar roofCannyHighInput = (SeekBar) ctx.findViewById(R.id.roofCannyHighThreshold);
    Integer roofCannyHigh = roofCannyHighInput.getProgress();

    TextView roofCannyApertureInput = (TextView) ctx.findViewById(R.id.roofCannyApertureSize);
    Integer roofCannyAperture = Integer.parseInt(roofCannyApertureInput.getText().toString());

    CheckBox roofCannyPreciseInput = (CheckBox) ctx.findViewById(R.id.roofCannyPreciseL2);
    boolean roofCannyPrecise = roofCannyPreciseInput.isChecked();

    TextView roofHoughResolutionInput = (TextView) ctx.findViewById(R.id.roofHoughResolution);
    Integer roofHoughResolution = Integer.parseInt(roofHoughResolutionInput.getText().toString());

    TextView roofHoughThetaInput = (TextView) ctx.findViewById(R.id.roofHoughTheta);
    Double roofHoughTheta = Double.parseDouble(roofHoughThetaInput.getText().toString());

    SeekBar roofHoughThresholdInput = (SeekBar) ctx.findViewById(R.id.roofHoughThreshold);
    Integer roofHoughThreshold = roofHoughThresholdInput.getProgress();

    SeekBar roofHoughMinLineLengthInput = (SeekBar) ctx.findViewById(R.id.roofHoughMinLineLength);
    Integer roofHoughMinLineLength = roofHoughMinLineLengthInput.getProgress();

    SeekBar roofHoughMaxLineGapInput = (SeekBar) ctx.findViewById(R.id.roofHoughMaxLineGap);
    Integer roofHoughMaxLineGap = roofHoughMaxLineGapInput.getProgress();

    TextView windowGaussWidthInput = (TextView) ctx.findViewById(R.id.windowKernelWidth);
    Integer windowGaussWidth = Integer.parseInt(windowGaussWidthInput.getText().toString());

    TextView windowGaussHeightInput = (TextView) ctx.findViewById(R.id.windowKernelHeight);
    Integer windowGaussHeight = Integer.parseInt(windowGaussHeightInput.getText().toString());

    TextView windowSigmaInput = (TextView) ctx.findViewById(R.id.windowSigma);
    Integer windowGaussSigma = Integer.parseInt(windowSigmaInput.getText().toString());

    SeekBar windowCannyLowInput = (SeekBar) ctx.findViewById(R.id.windowCannyLowThreshold);
    Integer windowCannyLow = windowCannyLowInput.getProgress();

    SeekBar windowCannyHighInput = (SeekBar) ctx.findViewById(R.id.windowCannyHighThreshold);
    Integer windowCannyHigh = windowCannyHighInput.getProgress();

    TextView windowCannyApertureInput = (TextView) ctx.findViewById(R.id.windowCannyApertureSize);
    Integer windowCannyAperture = Integer.parseInt(windowCannyApertureInput.getText().toString());

    CheckBox windowCannyPreciseInput = (CheckBox) ctx.findViewById(R.id.windowCannyPreciseL2);
    boolean windowCannyPrecise = windowCannyPreciseInput.isChecked();

    TextView windowHoughResolutionInput = (TextView) ctx.findViewById(R.id.windowHoughResolution);
    Integer windowHoughResolution = Integer.parseInt(windowHoughResolutionInput.getText().toString());

    EditText windowHoughThetaInput = (EditText) ctx.findViewById(R.id.windowHoughTheta);
    Double windowHoughTheta = Double.parseDouble(windowHoughThetaInput.getText().toString());

    SeekBar windowHoughThresholdInput = (SeekBar) ctx.findViewById(R.id.windowHoughThreshold);
    Integer windowHoughThreshold = windowHoughThresholdInput.getProgress();

    SeekBar windowHoughMinLineLengthInput = (SeekBar) ctx.findViewById(R.id.windowHoughMinLineLength);
    Integer windowHoughMinLineLength = windowHoughMinLineLengthInput.getProgress();

    SeekBar windowHoughMaxLineGapInput = (SeekBar) ctx.findViewById(R.id.windowHoughMaxLineGap);
    Integer windowHoughMaxLineGap = windowHoughMaxLineGapInput.getProgress();

    Canny<Mat> roofEdgeFilter = new Canny<Mat>(roofCannyLow, roofCannyHigh, roofCannyAperture, roofCannyPrecise);

    GaussianBlur<Mat> roofBlur = new GaussianBlur<>(roofGaussWidth, roofGaussHeight, roofGaussSigma);

    Hough<ImmutableList<Line>> roofLineDetector = new Hough<>(roofHoughResolution, Math.toRadians(roofHoughTheta), roofHoughThreshold, roofHoughMinLineLength, roofHoughMaxLineGap);

    GaussianBlur<Mat> windowBlur = new GaussianBlur<>(windowGaussWidth, windowGaussHeight, windowGaussSigma);
    Hough<ImmutableList<Line>> windowLineDetector = new Hough<>(windowHoughResolution, Math.toRadians(windowHoughTheta), windowHoughThreshold, windowHoughMinLineLength, windowHoughMaxLineGap);
    Canny<Mat> windowEdgeFilter = new Canny<>(windowCannyLow, windowCannyHigh, windowCannyAperture, windowCannyPrecise);

    return RoofArea.detectAll(RoofAreaCalculationActivity.img,
        roofEdgeFilter,
        windowEdgeFilter,
        roofBlur,
        windowBlur,
        roofLineDetector,
        windowLineDetector);
  }
}
