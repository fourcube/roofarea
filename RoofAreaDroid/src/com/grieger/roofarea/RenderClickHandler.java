package com.grieger.roofarea;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.grieger.roofarea.lib.imgproc.ColorConverter;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.util.HashMap;

public class RenderClickHandler implements Button.OnClickListener {
  Activity ctx;

  public RenderClickHandler(Activity ctx) {
    this.ctx = ctx;
  }

  @Override
  public void onClick(View v) {
    ImageView renderResult = (ImageView) ctx.findViewById(R.id.renderedImage);
    RoofAreaCalculator calc = new RoofAreaCalculator(ctx);
    HashMap<String, Mat> result = calc.apply();

    String field = "resultImage";
    Mat resultImage = result.get(field);
    resultImage = ColorConverter.BGRtoRGB(resultImage);

    Bitmap bitmap = Bitmap.createBitmap(resultImage.cols(), resultImage.rows(), Bitmap.Config.RGB_565);
    Utils.matToBitmap(resultImage, bitmap);

    renderResult.setImageBitmap(bitmap);

  }
}
