package com.grieger.roofarea;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import com.grieger.roofarea.lib.imgproc.ColorConverter;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import java.io.IOException;
import java.util.HashMap;

public class RoofAreaCalculationActivity extends Activity {

  public static HashMap<String, Mat> result;
  public static Mat img;
  public static String currentDetail = "";
  RoofAreaCalculator calc;

  // this is the action code we use in our intent,
  // this way we know we're looking at the response from our own action
  private static final int SELECT_PICTURE = 1;

  private String selectedImagePath;

  static {
    if (!OpenCVLoader.initDebug()) {
      // Handle initialization error
    }
  }

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    TabHost tabHost=(TabHost)findViewById(R.id.tabHost);
    tabHost.setup();

    TabSpec roofTab = tabHost.newTabSpec("Roof");
    roofTab.setContent(R.id.roofTab);
    roofTab.setIndicator("Roof");

    TabHost.TabSpec windowsTab = tabHost.newTabSpec("Windows");
    windowsTab.setContent(R.id.windowsTab);
    windowsTab.setIndicator("Windows");

    tabHost.addTab(roofTab);
    tabHost.addTab(windowsTab);

    ImageView renderResult = (ImageView) findViewById(R.id.renderedImage);
    try {

      img = Utils.loadResource(getApplicationContext(), R.drawable.input3, Highgui.CV_LOAD_IMAGE_ANYCOLOR);
      calc = new RoofAreaCalculator(this);
      HashMap<String, Mat> result = calc.apply();

      Mat resultImage = result.get("resultImage");
      resultImage = ColorConverter.BGRtoRGB(resultImage);
      Bitmap bitmap = Bitmap.createBitmap(resultImage.cols(), resultImage.rows(), Bitmap.Config.RGB_565);
      Utils.matToBitmap(resultImage, bitmap);

      renderResult.setImageBitmap(bitmap);
    } catch (IOException e) {
      e.printStackTrace();
    }

    Button render = (Button) findViewById(R.id.renderButton);
    render.setOnClickListener(new RenderClickHandler(this));

    Button roofBlur = (Button) findViewById(R.id.roofShowBlur);
    roofBlur.setOnClickListener(new DisplaySingleClickHandler("roofBlur", this));

    Button roofEdge = (Button) findViewById(R.id.roofShowEdge);
    roofEdge.setOnClickListener(new DisplaySingleClickHandler("roofEdge", this));

    Button roofLine = (Button) findViewById(R.id.roofShowLine);
    roofLine.setOnClickListener(new DisplaySingleClickHandler("roofLine", this));

    Button windowBlur = (Button) findViewById(R.id.windowShowBlur);
    windowBlur.setOnClickListener(new DisplaySingleClickHandler("windowBlur", this));

    Button windowEdge = (Button) findViewById(R.id.windowShowEdge);
    windowEdge.setOnClickListener(new DisplaySingleClickHandler("windowEdge", this));

    Button windowLine = (Button) findViewById(R.id.windowShowLine);
    windowLine.setOnClickListener(new DisplaySingleClickHandler("windowLine", this));

    ((Button) findViewById(R.id.loadImage))
        .setOnClickListener(new View.OnClickListener() {

          public void onClick(View arg0) {

            // in onCreate or any event where your want the user to
            // select a file
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);
          }
        });
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    ImageView renderResult = (ImageView) findViewById(R.id.renderedImage);

    if (resultCode == RESULT_OK) {
      if (requestCode == SELECT_PICTURE) {
        Uri selectedImageUri = data.getData();
        selectedImagePath = getPath(selectedImageUri);

        img = Highgui.imread(selectedImagePath);
        HashMap<String, Mat> result = calc.apply();

        Mat resultImage = result.get("resultImage");
        resultImage = ColorConverter.BGRtoRGB(resultImage);
        Bitmap bitmap = Bitmap.createBitmap(resultImage.cols(), resultImage.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(resultImage, bitmap);

        renderResult.setImageBitmap(bitmap);
      }
    }
  }

  /**
   * helper to retrieve the path of an image URI
   */
  public String getPath(Uri uri) {
    // just some safety built in
    if( uri == null ) {
      // TODO perform some logging or show user feedback
      return null;
    }
    // try to retrieve the image from the media store first
    // this will only work for images selected from gallery
    String[] projection = { MediaStore.Images.Media.DATA };
    Cursor cursor = managedQuery(uri, projection, null, null, null);
    if( cursor != null ){
      int column_index = cursor
          .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      return cursor.getString(column_index);
    }
    // this is our fallback here
    return uri.getPath();
  }



}
