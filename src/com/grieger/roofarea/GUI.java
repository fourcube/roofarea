package com.grieger.roofarea;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Created by Chris on 23.06.2014.
 */
public class GUI {
    private static int openWindows = 0;

    public static void showResult(Mat img, String title) {
        Imgproc.resize(img, img, new Size(img.width(), img.height()));
        MatOfByte matOfByte = new MatOfByte();
        Highgui.imencode(".jpg", img, matOfByte);

        byte[] byteArray = matOfByte.toArray();
        BufferedImage bufImage = null;
        try {
            InputStream in = new ByteArrayInputStream(byteArray);
            bufImage = ImageIO.read(in);
            JFrame frame = new JFrame(title);
            frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
            frame.pack();
            frame.setVisible(true);
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            frame.addWindowStateListener(new WindowStateListener() {
                @Override
                public void windowStateChanged(WindowEvent e) {
                    if(e.getNewState() == WindowEvent.WINDOW_CLOSED) {
                        openWindows--;
                        if(openWindows <= 0) {
                            System.exit(0);
                        }
                    }
                }
            });
            openWindows++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
