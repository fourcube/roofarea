package com.grieger.roofarea;

import com.google.common.collect.ImmutableList;
import com.grieger.roofarea.lib.RoofArea;
import com.grieger.roofarea.lib.imgproc.Canny;
import com.grieger.roofarea.lib.imgproc.GaussianBlur;
import com.grieger.roofarea.lib.imgproc.Hough;
import com.grieger.roofarea.lib.primitives.Line;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;

import java.util.HashMap;

public class Main {
  static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }

  public static void main(String[] args) {
    //detectAll("input.jpg");
    //detectAll("input2.jpg");
    //detect("input3.jpg");
    detect("input4.jpg");
    //detect("input3.jpg");
    //detectAll("IMG_0015.JPG");
  }

  public static void detect(String filename) {
    Canny<Mat> roofEdgeFilter = new Canny<>(600, 980, 5, true);
    Canny<Mat> windowEdgeFilter = new Canny<>(1200, 1900, 5, true);


    GaussianBlur<Mat> roofBlur = new GaussianBlur<>(9, 9, 2);
    GaussianBlur<Mat> windowBlur = new GaussianBlur<>(5, 5, 2);

    Hough<ImmutableList<Line>> roofLineDetector = new Hough<>(1, Math.toRadians(0.3), 200, 90, 580);
    Hough<ImmutableList<Line>> windowLineDetector = new Hough<>(1, Math.toRadians(0.2), 60, 20, 15);

    Mat image = Highgui.imread(filename);


    HashMap<String, Mat> res = RoofArea.detectAll(image, roofEdgeFilter, windowEdgeFilter, roofBlur, windowBlur, roofLineDetector, windowLineDetector);

    //GUI.showResult(res.get("windowLine"), "TEST");
    GUI.showResult(res.get(""), "TEST");
  }

}