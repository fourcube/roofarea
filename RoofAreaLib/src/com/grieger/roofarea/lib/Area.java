package com.grieger.roofarea.lib;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.grieger.roofarea.lib.primitives.*;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import java.util.List;

public class Area {
    public static double calculateArea(Mat image,
                                       ImmutableList<Line> horizontalWindowLines,
                                       ImmutableList<Line> verticalWindowLines,
                                       ImmutableList<Line> horizontalRoofLines,
                                       ImmutableList<Line> verticalRoofLines,
                                       double vLength,
                                       double hLength) {

        if(!(horizontalRoofLines.size() > 0 && verticalRoofLines.size() > 0 &&
                horizontalWindowLines.size() > 0 && verticalWindowLines.size() > 0)) {
            return 0.0f;
        }

        final Scalar CYAN = new Scalar(255,255,0);

        List<Line> orderedHorizontalRoofLines = Ordering.from(new LineBottomToTopComparator()).immutableSortedCopy(horizontalRoofLines);
        List<Line> orderedHorizontalWindowLines = Ordering.from(new LineBottomToTopComparator()).immutableSortedCopy(horizontalWindowLines);

        List<Line> orderedVerticalRoofLines = Ordering.from(new LineLeftToRightComparator()).immutableSortedCopy(verticalRoofLines);
        List<Line> orderedVerticalWindowLines = Ordering.from(new LineLeftToRightComparator()).immutableSortedCopy(verticalWindowLines);


        Line windowLineHorizontal = orderedHorizontalWindowLines.get(0);
        System.out.println("horizontal Window: " + windowLineHorizontal);
        double hRatio = windowLineHorizontal.length() / hLength;
        System.out.println("hRatio: " + hRatio);

        Core.line(image, windowLineHorizontal.p1, windowLineHorizontal.p2, new Scalar(0,0,255), 5);

        Line windowLineVertical = orderedVerticalWindowLines.get(0);
        System.out.println("vertical Window: " + windowLineVertical);
        double vRatio = windowLineVertical.length() / vLength;
        System.out.println("vRatio: " + vRatio);

        Core.line(image, windowLineVertical.p1, windowLineVertical.p2, CYAN, 5);

        Line roofLineHorizontal = orderedHorizontalRoofLines.get(0);
        System.out.println(roofLineHorizontal);
        double lRoofHorizontal = roofLineHorizontal.length() / hRatio;

        Core.line(image, roofLineHorizontal.p1, roofLineHorizontal.p2, new Scalar(0,0,255), 5);

        Line roofLineVertical = orderedVerticalRoofLines.get(0);
        System.out.println(roofLineVertical);
        double lRoofVertical = roofLineVertical.length() / vRatio;

        Core.line(image, roofLineVertical.p1, roofLineVertical.p2, CYAN, 5);


        return (lRoofHorizontal * lRoofVertical) / 10000;
    }
}
