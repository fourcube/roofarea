package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Mat;

public interface Applicable<T> {
    public T applyTo(Mat input);
}
