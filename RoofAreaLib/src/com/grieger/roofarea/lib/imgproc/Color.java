package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Scalar;

public interface Color {
    public static final Scalar RED = new Scalar(0,0,255);
    public static final Scalar GREEN = new Scalar(0,255,0);
    public static final Scalar BLUE = new Scalar(255,0,0);
}
