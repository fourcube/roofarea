package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

public class Canny<T> implements ApplicableMasked<T> {
  private final int lowThreshold; // = 670;
  private final int highThreshold; // = 980;
  private int apertureSize = 0; //= 5;
  private boolean preciseL2 = false; // = true;

  public Canny(int lowThreshold, int highThreshold, int apertureSize, boolean preciseL2) {
    this.lowThreshold = lowThreshold;
    this.highThreshold = highThreshold;
    this.apertureSize = apertureSize;
    this.preciseL2 = preciseL2;
  }

  public Canny(int lowThreshold, int highThreshold) {
    this.lowThreshold = lowThreshold;
    this.highThreshold = highThreshold;
  }

  @SuppressWarnings("unchecked")
  public T applyTo(Mat input) {
    Mat result = input.clone();
    if(apertureSize == 0) {
      Imgproc.Canny(input, result, lowThreshold, highThreshold);
    } else {
      Imgproc.Canny(input, result, lowThreshold, highThreshold, apertureSize, preciseL2);
    }
    return (T) result;
  }

  @SuppressWarnings("unchecked")
  public T applyTo(Mat input, Mat mask) {
    Mat result = input.clone();
    Mat cutResult = Mat.zeros(input.size(), input.type());

    if(apertureSize == 0) {
      Imgproc.Canny(input, result, lowThreshold, highThreshold);
    } else {
      Imgproc.Canny(input, result, lowThreshold, highThreshold, apertureSize, preciseL2);
    }

    Mat scaledMask = new Mat(mask.size(), CvType.CV_8UC1);
    Mat scale = Imgproc.getRotationMatrix2D(new Point(mask.width() / 2, mask.height() / 2), 0, 0.7);

    Imgproc.warpAffine(mask, scaledMask, scale, mask.size());

    result.copyTo(cutResult, scaledMask);
    return (T) cutResult;
  }

}
