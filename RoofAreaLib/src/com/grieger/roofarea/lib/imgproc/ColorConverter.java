package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class ColorConverter {
  public static Mat RGBToGrey(Mat input) {
    Mat result = input.clone();
    Imgproc.cvtColor(input, result, Imgproc.COLOR_RGB2GRAY);
    return result;
  }

  public static Mat BGRtoRGB(Mat input) {
    Mat result = input.clone();
    Imgproc.cvtColor(input, result, Imgproc.COLOR_BGR2RGB);
    return result;
  }

  public static Mat greyToRGB(Mat input) {
    Mat result = input.clone();
    Imgproc.cvtColor(input, result, Imgproc.COLOR_GRAY2RGB);
    return result;
  }
}
