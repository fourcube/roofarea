package com.grieger.roofarea.lib.imgproc;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.grieger.roofarea.lib.primitives.Line;
import com.grieger.roofarea.lib.primitives.LineLeftToRightComparator;
import com.grieger.roofarea.lib.primitives.LineLengthComparator;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindWindows {
  /**
   * Find the two longest horizontal lines in the input set
   * @return
   */
  public static ImmutableList<Line> verticalRoofLines(ImmutableList<Line> lines) {
    List<Line> verticalLines = new ArrayList<>();
    List<Line> sortedLines = Ordering.from(new LineLengthComparator()).sortedCopy(lines);
    List<Line> result = new ArrayList<>();

    for(Line l: sortedLines) {
      double angle = l.angle();

      if(angle > Math.toRadians(30)) {
        verticalLines.add(l);
      }
    }

    double averageLength = Line.averageLength(verticalLines);

    // get the 2 longest and highest lines
    List<Line> sorted = Ordering.from(new LineLeftToRightComparator()).sortedCopy(verticalLines);

    System.out.println("Lines:");
    for(Line l: sorted) {
      System.out.println(l);

    }

    System.out.println("Done");

    for(Line l: sorted) {
      if(result.size() == 2) {
        break;
      }

      Line previous = result.isEmpty() ? null : result.get(0);
      double distance = Double.MAX_VALUE;

      if(previous != null) {
        distance = Math.abs(previous.p1.x - l.p1.x);
      }

      System.out.println(distance + " - " + l.length() + " - (" + averageLength + ")");

      if(l.length() > (averageLength * 0.8)  && distance > 15) {
        System.out.println("Found possible vertical window line: " + l.toString());
        result.add(l);
      }
    }
    return ImmutableList.copyOf(result);
  }

  /**
   * Find all window lines based on some vertical lines
   * @return
   */
  public static ImmutableList<Line> allWindowLines(ImmutableList<Line> lines, final ImmutableList<Line> verticalLines) {
    ArrayList<Line> result = new ArrayList<>();

    if(verticalLines.size() < 2) {
      return ImmutableList.copyOf(result);
    }

    Line h1 = verticalLines.get(0),
        h2 = verticalLines.get(1),
        horizontalLine = null;

    Predicate<Line> verticalLinePredicate = new Predicate<Line>() {
      @Override
      public boolean apply(Line l) {
        return !verticalLines.contains(l) && l.angle() < Math.toRadians(20);
      }
    };

    List<Line> filteredLines = Lists.newArrayList(Iterables.filter(lines, verticalLinePredicate));

    for(Line l: filteredLines) {
      if(l.intersects2(h1, h2)) {
        horizontalLine = l;
        break;
      }
    }

    if(horizontalLine == null) {
      // Just guess
      horizontalLine = new Line(h1.p1, h2.p1);
    }

    Line leftEdge = h1;
    Line rightEdge = h2;

    Line edgeA, edgeB, edgeC, edgeD;

    if(leftEdge.length() >= rightEdge.length()) {
      edgeA = leftEdge;
      edgeB = rightEdge;
    } else {
      edgeA = rightEdge;
      edgeB = leftEdge;
    }

    edgeC = new Line(edgeA.p1, edgeB.p1);

    Point intermediatePoint = new Point(horizontalLine.p1.x, edgeA.p2.y);
    Line intermediateEdgeD = new Line(edgeA.p2, intermediatePoint);
    Point intersectionPointD = Line.intersectionPoint(intermediateEdgeD, edgeB);
    edgeD = new Line(edgeA.p2, intersectionPointD);

    Line intermediateEdgeB = new Line(edgeB.p1, edgeD.p2);
    Point intersectionPointB = Line.intersectionPoint(intermediateEdgeB, edgeD);

    edgeB = new Line(intersectionPointB, edgeB.p1);

    result.addAll(Arrays.asList(edgeA, edgeB, edgeC, edgeD));

    return ImmutableList.copyOf(result);
  }


}
