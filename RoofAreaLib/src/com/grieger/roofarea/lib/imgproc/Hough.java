package com.grieger.roofarea.lib.imgproc;

import com.google.common.collect.ImmutableList;
import com.grieger.roofarea.lib.primitives.Line;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

public class Hough<T extends ImmutableList<Line>> implements Applicable {
  private final int resolutionPx;// = 2;
  private final double theta;// = Math.PI / 140;
  private final int threshold;// = 200;
  private final int minLineLength;// = 60;
  private final int maxLineGap;// = 380;

  public Hough(int resolutionPx, double theta, int threshold, int minLineLength, int maxLineGap) {
    this.resolutionPx = resolutionPx;
    this.theta = theta;
    this.threshold = threshold;
    this.minLineLength = minLineLength;
    this.maxLineGap = maxLineGap;
  }

  @SuppressWarnings("unchecked")
  public T applyTo(Mat input) {
    Mat linesVector = input.clone();
    ImmutableList.Builder<Line> builder = ImmutableList.builder();

    Imgproc.HoughLinesP(input, linesVector, resolutionPx, theta, threshold, minLineLength, maxLineGap);

    for (int i = 0; i < linesVector.cols() && !linesVector.empty(); i++) {
      double[] v = linesVector.get(0, i);

      Point p1 = new Point(v[0], v[1]);
      Point p2 = new Point(v[2], v[3]);
      builder.add(new Line(p1, p2));
    }

    return (T) builder.build();
  }
}
