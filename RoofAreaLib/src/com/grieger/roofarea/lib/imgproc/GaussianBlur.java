package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class GaussianBlur<T> implements Applicable{
    private final int kernelRows;// = 9;
    private final int kernelColumns;// = 9;
    private final int sigma;// = 2;

    public GaussianBlur(int kernelRows, int kernelColumns, int sigma) {
        this.kernelRows = kernelRows;
        this.kernelColumns = kernelColumns;
        this.sigma = sigma;
    }

    @SuppressWarnings("unchecked")
    public T applyTo(Mat input) {
        Mat result = input.clone();
        Imgproc.GaussianBlur(input, result, new Size(kernelRows, kernelColumns), sigma);
        return (T) result;
    }


}
