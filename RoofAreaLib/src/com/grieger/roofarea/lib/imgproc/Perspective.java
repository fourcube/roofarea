package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.util.List;

public class Perspective {
  public static Mat applyCorrectionTo(Mat input, List<Point> roi) {
    if(roi.isEmpty()) {
      return input;
    }

    Mat result = Mat.zeros(input.size(), input.type());

    Mat target_points = new MatOfPoint2f(
        new Point(result.cols() - 1, result.rows() - 1),
        new Point(0, result.rows() - 1),
        new Point(0,0),
        new Point(result.cols() -1, 0)
    );
    System.out.println("Perspective transform: " + roi.size());

    for(Point p: roi) {
      System.out.println(p);
    }

    System.out.println(target_points.get(0,0)[0] + " - " + target_points.get(0,0)[1]);
    System.out.println(target_points.get(1,0)[0] + " - " + target_points.get(1,0)[1]);
    System.out.println(target_points.get(2,0)[0] + " - " + target_points.get(2,0)[1]);
    System.out.println(target_points.get(3,0)[0] + " - " + target_points.get(3,0)[1]);


    MatOfPoint2f points = new MatOfPoint2f(roi.toArray(new Point[roi.size()]));
    Mat transform = Imgproc.getPerspectiveTransform(points, target_points);

    Imgproc.warpPerspective(input, result, transform, result.size());

    return result;
  }
}
