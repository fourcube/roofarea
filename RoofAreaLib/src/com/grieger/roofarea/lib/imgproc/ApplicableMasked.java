package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.Mat;

public interface ApplicableMasked<T> extends Applicable {
    public T applyTo(Mat input, Mat mask);
}
