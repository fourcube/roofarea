package com.grieger.roofarea.lib.imgproc;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.grieger.roofarea.lib.primitives.Line;
import com.grieger.roofarea.lib.primitives.LineLengthComparator;
import com.grieger.roofarea.lib.primitives.LineTopToBottomComparator;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindRoof {
  /**
   * Find the two longest horizontal lines in the input set
   * @return
   */
  public static ImmutableList<Line> horizontalRoofLines(ImmutableList<Line> lines) {
    ArrayList<Line> horizontalLines = new ArrayList<>();
    List<Line> sortedLines = Ordering.from(new LineLengthComparator()).sortedCopy(lines);
    ArrayList<Line> result = new ArrayList<>();

    for(Line l: sortedLines) {
      double angle = l.angle();

      if(angle < Math.toRadians(30)) {
        horizontalLines.add(l);
      }
    }

    double averageLength = Line.averageLength(sortedLines);

    // get the 2 longest and highest lines
    List<Line> sorted = Ordering.from(new LineTopToBottomComparator()).sortedCopy(horizontalLines);

    for(Line l: sorted) {
      if(result.size() == 2) {
        break;
      }

      Line previous = result.isEmpty() ? null : result.get(0);
      double distance = Double.MAX_VALUE;

      if(previous != null) {
        distance = Math.abs(previous.p1.y - l.p1.y);
      }

      if(l.length() >= averageLength / 2 && distance > averageLength / 3) {
        System.out.println("Found possible high roof line: " + l.toString());
        result.add(l);
      }
    }
    return ImmutableList.copyOf(result);
  }

  /**
   * Find all roof lines based on some horizontal lines
   * @return
   */
  public static ImmutableList<Line> allRoofLines(ImmutableList<Line> lines, final ImmutableList<Line> horizontalLines) {
    ArrayList<Line> result = new ArrayList<>();
    if(horizontalLines.size() < 2) {
      return ImmutableList.copyOf(result);
    }

    Line h1 = horizontalLines.get(0),
        h2 = horizontalLines.get(1),
        verticalLine = null;

    Predicate<Line> verticalLinePredicate = new Predicate<Line>() {
      @Override
      public boolean apply(Line l) {
        return !horizontalLines.contains(l) && l.angle() > Math.toRadians(30);
      }
    };

    List<Line> filteredLines =  Lists.newArrayList(Iterables.filter(lines, verticalLinePredicate));

    for(Line l: filteredLines) {
      if(l.intersects2(h1, h2)) {
        verticalLine = l;
        break;
      }
    }

    if(verticalLine == null) {
      // Just guess
      verticalLine = new Line(h1.p1, h2.p1);
    }

    Point intersectionTop = Line.intersectionPoint(verticalLine, h1);
    Point intersectionBottomLeft = Line.intersectionPoint(verticalLine, h2);

    Line leftRoofEdge = new Line(intersectionTop, intersectionBottomLeft);
    Line topRoofEdge = new Line(intersectionTop, h1.p2);

    Line intermediateBottomRoofEdge = new Line(intersectionBottomLeft, h2.p2);
    Line intermediateRightEdge = new Line(h1.p2, h2.p2);

    Point intersectionBottomRight = Line.intersectionPoint(intermediateRightEdge, intermediateBottomRoofEdge);
    Line rightRoofEdge = new Line(h1.p2, intersectionBottomRight);
    Line bottomRoofEdge = new Line(intersectionBottomLeft, intersectionBottomRight);


    result.addAll(Arrays.asList(leftRoofEdge, rightRoofEdge, bottomRoofEdge, topRoofEdge));


    return ImmutableList.copyOf(result);
  }

  public static List<Point> convexPoints(ImmutableList<Line> lines) {
    if(lines.size() < 4) {
      return Lists.newArrayList();
    }

    MatOfPoint hull;
    MatOfInt indices = new MatOfInt();

    ArrayList<Point> points = new ArrayList<>();
    ArrayList<Point> convexPoints = new ArrayList<>();

    for(Line l: lines) {
      points.addAll(Arrays.asList(l.p1, l.p2));
    }

    hull = new MatOfPoint(points.toArray(new Point[points.size()]));
    Imgproc.convexHull(hull, indices);


    for(int i: indices.toList()) {
      convexPoints.add(points.get(i));
    }

    return convexPoints;
  }
  public static Mat roofMask(Mat input, ImmutableList<Line> lines) {
    MatOfPoint2f poly;
    MatOfPoint2f approx = new MatOfPoint2f();
    MatOfPoint approxPoint;
    Mat mask = Mat.zeros(input.size(), CvType.CV_8UC1);

    List<Point> convexPoints = convexPoints(lines);

    if(convexPoints.isEmpty()) {
      return mask;
    }

    poly = new MatOfPoint2f(convexPoints.toArray(new Point[convexPoints.size()]));
    Imgproc.approxPolyDP(poly, approx, 1.0, true);

    approxPoint = new MatOfPoint(approx.toArray());
    Core.fillConvexPoly(mask, approxPoint, new Scalar(255,255,255));

    return mask;
  }

  public static Mat extractRoof(Mat input, Mat mask) {

    Mat result = Mat.zeros(input.size(), CvType.CV_8UC1);


    input.copyTo(result, mask);
    return result;
  }


}
