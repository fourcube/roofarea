package com.grieger.roofarea.lib.imgproc;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

public class ScaledMaskExtractor<T> implements ApplicableMasked<T> {
    double scale;

    public ScaledMaskExtractor(double scale) {
        this.scale = scale;
    }


    @Override
    public T applyTo(org.opencv.core.Mat input, org.opencv.core.Mat mask) {
        org.opencv.core.Mat result = input.clone();
        org.opencv.core.Mat cutResult = org.opencv.core.Mat.zeros(input.size(), input.type());

        org.opencv.core.Mat scaledMask = new org.opencv.core.Mat(mask.size(), CvType.CV_8UC1);
        org.opencv.core.Mat scale = Imgproc.getRotationMatrix2D(new Point(mask.width() / 2, mask.height() / 2), 0, 0.7);

        Imgproc.warpAffine(mask, scaledMask, scale, mask.size());

        result.copyTo(cutResult, scaledMask);
        return (T) cutResult;
    }

    @Override
    public T applyTo(org.opencv.core.Mat input) {
        return applyTo(input, input);
    }
}
