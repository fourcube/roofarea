package com.grieger.roofarea.lib.predicates;

import com.google.common.base.Predicate;
import com.grieger.roofarea.lib.primitives.Line;

import java.util.List;

public class Predicates<T extends Line,U extends List<T>> {
  public Predicate<T> notIn(final U list) {
    return new Predicate<T>() {
      @Override
      public boolean apply(T t) {
        System.out.println(t);
        System.out.println("In list:" + list.contains(t));
        return !list.contains(t);
      }
    };
  }

  public Predicate<T> vertical() {
    return new Predicate<T>() {
      @Override
      public boolean apply(T t) {
        return t.angle() > Math.toRadians(30);
      }
    };
  }

  public Predicate<T> horizontal() {
    return new Predicate<T>() {
      @Override
      public boolean apply(T t) {
        return t.angle() < Math.toRadians(20);
      }
    };
  }
}
