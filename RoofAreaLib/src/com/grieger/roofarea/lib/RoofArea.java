package com.grieger.roofarea.lib;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.grieger.roofarea.lib.imgproc.*;
import com.grieger.roofarea.lib.predicates.Predicates;
import com.grieger.roofarea.lib.primitives.Line;
import com.grieger.roofarea.lib.primitives.LineLengthComparator;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;

public class RoofArea {
  public final static Scalar RED = new Scalar(0,0,255);
  public final static Scalar GREEN = new Scalar(0,255,0);
  public final static Scalar BLUE = new Scalar(255,0,0);
  public final static Scalar BLACK = new Scalar(0,0,0);
  public final static Scalar CYAN = new Scalar(255,255,0);

  public static HashMap<String, Mat> detectAll(Mat input,
                                               Canny<Mat> roofEdgeFilter,
                                               Canny<Mat> windowEdgeFilter,
                                               GaussianBlur<Mat> roofBlur,
                                               GaussianBlur<Mat> windowBlur,
                                               Hough<ImmutableList<Line>> roofLineDetector,
                                               Hough<ImmutableList<Line>> windowLineDetector) {

    HashMap<String, Mat> result = new HashMap<>();

    Mat image,
        greyImage,
        blurredImage,
        edgeImage,
        roofMaskImage,
        roofOnlyImage;

    ImmutableList<Line> roofLines,
        windowLines;

    Predicates<Line, ImmutableList<Line>> linePredicates = new Predicates();

    image = Mat.zeros(input.size(), input.type());
    // Resize the image to a 3rd of its original size
    Imgproc.resize(input, image, new Size(image.width() / 3, image.height() / 3));

    // Convert to a grey image
    greyImage = ColorConverter.RGBToGrey(image);

    // Blur it so the edge filter works better
    blurredImage = roofBlur.applyTo(greyImage);

    result.put("roofBlur", blurredImage);

    // Filter for edgeImage
    edgeImage = roofEdgeFilter.applyTo(blurredImage);

    result.put("roofEdge", edgeImage);

    // Detect detectedLines inside the edge picture
    roofLines = roofLineDetector.applyTo(edgeImage);

    Mat roofLineImage = ColorConverter.greyToRGB(edgeImage.clone());

    for(Line l: roofLines) {
      Core.line(roofLineImage, l.p1, l.p2, RED, 2);
    }

    result.put("roofLine", roofLineImage);

    //edgeImage = Canny.applyWindows(blurredImage);
        /*for(Line l: detectedLines) {
            Core.line(image, l.p1, l.p2, RED, 3);
        }
        GUI.showResult(edgeImage, "Hough edgeImage - " + imageFile);*/

    ImmutableList<Line> preliminaryHorizontalRoofLines = FindRoof.horizontalRoofLines(roofLines);
    System.out.println(preliminaryHorizontalRoofLines.size());
    ImmutableList<Line> convexRoofLines = FindRoof.allRoofLines(roofLines, preliminaryHorizontalRoofLines);

    ImmutableList<Line> verticalRoofLines = ImmutableList.copyOf(
        Ordering.from(new LineLengthComparator())
            .sortedCopy(Iterables.filter(convexRoofLines, linePredicates.vertical())));

    ImmutableList<Line> horizontalRoofLines = ImmutableList.copyOf(
        Ordering.from(new LineLengthComparator())
            .sortedCopy(Iterables.filter(convexRoofLines, linePredicates.horizontal())));


    Mat guiImage = image.clone();

    for(Line l: convexRoofLines) {
      Core.line(guiImage, l.p1, l.p2, GREEN, 3);
    }

    roofMaskImage = FindRoof.roofMask(image, convexRoofLines);
    roofOnlyImage = FindRoof.extractRoof(image, roofMaskImage);

    List<Point> convexPoints = FindRoof.convexPoints(convexRoofLines);
    Mat unwarped = Perspective.applyCorrectionTo(roofOnlyImage, convexPoints);

    //GUI.showResult(roofOnly, "Roof only - " + imageFile);
    Mat roofOnlyBlurredImage = windowBlur.applyTo(roofOnlyImage);
    result.put("windowBlur", roofOnlyBlurredImage);

    Mat roofOnlyEdgeImage = windowEdgeFilter.applyTo(roofOnlyBlurredImage, roofMaskImage);
    result.put("windowEdge", roofOnlyEdgeImage);

    windowLines = windowLineDetector.applyTo(roofOnlyEdgeImage);

    Mat windowLineImage = ColorConverter.greyToRGB(roofOnlyEdgeImage.clone());

    for(Line l: windowLines) {
      Core.line(windowLineImage, l.p1, l.p2, RED, 2);
    }

    result.put("windowLine", windowLineImage);

    ImmutableList<Line> preliminaryVerticalWindowLines = FindWindows.verticalRoofLines(windowLines);
    ImmutableList<Line> convexWindowLines = FindWindows.allWindowLines(windowLines, preliminaryVerticalWindowLines);
    ImmutableList<Line> horizontalWindowLines = ImmutableList.copyOf(
        Ordering.from(new LineLengthComparator())
            .sortedCopy(Iterables.filter(convexWindowLines, linePredicates.horizontal()))
    );

    ImmutableList<Line> verticalWindowLines = ImmutableList.copyOf(
        Ordering.from(new LineLengthComparator())
            .sortedCopy(Iterables.filter(convexWindowLines, linePredicates.vertical()))
    );

    double area = Area.calculateArea(guiImage, horizontalWindowLines, verticalWindowLines, horizontalRoofLines ,verticalRoofLines, 270, 80);

    for(Line l: convexWindowLines) {
      Core.line(guiImage, l.p1, l.p2, BLUE, 2);
    }

    System.out.println(area + "sqm");

    NumberFormat nf = new DecimalFormat("0.000");
    Core.putText(guiImage, "Area: " + nf.format(area) + " sqm", new Point(10, 50), Core.FONT_HERSHEY_COMPLEX, 1.1, BLACK);
    result.put("resultImage", guiImage);

    return result;
  }


}
