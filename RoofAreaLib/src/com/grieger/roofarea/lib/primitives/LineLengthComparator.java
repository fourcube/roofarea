package com.grieger.roofarea.lib.primitives;

import java.util.Comparator;

public class LineLengthComparator implements Comparator<Line> {
  @Override
  public int compare(Line a, Line b) {
    if(a.length() > b.length()) return -1;
    if(a.length() < b.length()) return 1;
    return 0;
  }
}
