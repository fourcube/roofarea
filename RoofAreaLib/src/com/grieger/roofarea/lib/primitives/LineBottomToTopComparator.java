package com.grieger.roofarea.lib.primitives;

import java.util.Comparator;

public class LineBottomToTopComparator implements Comparator<Line> {
  @Override
  public int compare(Line a, Line b) {
    if(a.yAverage() > b.yAverage()) return -1;
    if(a.yAverage() < b.yAverage()) return 1;
    return 0;
  }
}
