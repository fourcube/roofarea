package com.grieger.roofarea.lib.primitives;

import java.util.Comparator;

public class LineLeftToRightComparator implements Comparator<Line> {
  @Override
  public int compare(Line a, Line b) {
    if(a.xAverage() < b.xAverage()) return -1;
    if(a.xAverage() > b.xAverage()) return 1;
    return 0;
  }
}
