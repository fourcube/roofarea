package com.grieger.roofarea.lib.primitives;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.opencv.core.Point;

import java.util.List;

public class Line {
  public Point p1;
  public Point p2;

  public Line(Point p1, Point p2) {
    this.p1 = p1;
    this.p2 = p2;
  }

  /**
   *
   * @return Euclidean distance between the line's points
   */
  public double length() {
    return euclideanDistance(p1, p2);
  }

  public static double euclideanDistance(Point p1, Point p2) {
    if(p1 == null || p2 == null) return 0;

    return Math.sqrt(
        (p1.x - p2.x) * (p1.x - p2.x) +
            (p1.y - p2.y) * (p1.y - p2.y));
  }

  /**
   *
   * @return Angle between p1 and p2 in radians
   */
  public double angle() {
    if(p2 == null || p1 == null) return 0;
    Point x0 = new Point(p2.x, p1.y);

    double b = euclideanDistance(x0, p2);
    double c = this.length();

    return Math.asin(b / c);
  }

  public static int sortLength(Line a, Line b) {
    if(a.length() > b.length()) return -1;
    if(a.length() < b.length()) return 1;
    return 0;
  }

  public static double averageLength(List<Line> lines) {
    Function<Line, Double> mapLength = new Function<Line, Double>() {
      @Override
      public Double apply(Line line) {
        return line.length();
      }
    };

    List<Double> lengths = Lists.transform(lines, mapLength);

    double averageLength = 0.0f;
    for(Double d: lengths) {
      averageLength += d;
    }
    return averageLength / lengths.size();
  }
  public double yAverage() {
    return Math.abs((p2.y + p1.y) / 2.0f);
  }

  public double xAverage() {
    return Math.abs((p2.x + p1.x) / 2.0f);
  }

  public boolean intersects(Line other)
  {
    Point x = new Point(other.p1.x - this.p1.x, other.p1.y - this.p1.y);
    Point d1 = new Point(this.p2.x - this.p1.x, this.p2.y - this.p1.y);
    Point d2 = new Point(other.p2.x - other.p1.x, other.p2.y - other.p1.y);


    double cross = d1.x*d2.y - d1.y*d2.x;
    if (Math.abs(cross) < 1e-8) {
      return false;
    }

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    return true;
  }

  public boolean intersects2(Line other1, Line other2)
  {
    return intersects(other1) && intersects(other2);
  }

  @Override
  public String toString() {
    return "Line{" +
        "p1=" + p1 +
        ", p2=" + p2 +
        "} angle = " + this.angle() + " length = " + this.length();
  }

  /**
   * Computes the intersection between two lines. The calculated point is approximate,
   * since integers are used. If you need a more precise result, use doubles
   * everywhere.
   * (c) 2007 Alexander Hristov. Use Freely (LGPL license). http://www.ahristov.com
   *
   */
  public static Point intersectionPoint(Line l1, Line l2) {
    double x1 = l1.p1.x;
    double y1 = l1.p1.y;
    double x2 = l1.p2.x;
    double y2 = l1.p2.y;
    double x3 = l2.p1.x;
    double y3 = l2.p1.y;
    double x4 = l2.p2.x;
    double y4 = l2.p2.y;
    double d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (d == 0) return null;

    double xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
    double yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;

    return new Point(xi,yi);
  }

  public double dot(Line o) {
    double x1 = p2.x - p1.x;
    double y1 = p2.y - p1.y;

    double x2 = o.p2.x - o.p1.x;
    double y2 = o.p2.y - o.p1.y;

    return (x1 * x2) + (y1 * y2);
  }

  public double angle(Line o) {
    double x1 = p2.x - p1.x;
    double y1 = p2.y - p1.y;

    double x2 = o.p2.x - o.p1.x;
    double y2 = o.p2.y - o.p1.y;

    return Math.acos(this.dot(o) / (Math.sqrt((x1 * x1) + (y1 * y1)) * Math.sqrt((x2 * x2) + (y2 * y2))));
  }

  public static Point norm(Point p) {
    double len = Math.sqrt((p.x*p.x) + (p.y * p.y));
    return new Point(p.x / len, p.y / len);
  }

  public static Point mul(Point p, double c) {
    return new Point(p.x + c, p.y * c);
  }

  public static Point sub(Point a, Point b) {
    return new Point(a.x - b.x, a.y - b.y);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Line line = (Line) o;

    if (p1.x != line.p1.x ||
        p1.y != line.p1.y ||
        p2.x != line.p2.x ||
        p2.y != line.p2.y) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = p1 != null ? p1.hashCode() : 0;
    result = 31 * result + (p2 != null ? p2.hashCode() : 0);
    return result;
  }
}
